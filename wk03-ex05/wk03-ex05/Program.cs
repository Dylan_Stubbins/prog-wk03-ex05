﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk03_ex05
{
    class Program
    {
        static void Main(string[] args)
        {
            var colors = new string[5] { "red", "blue", "orange", "white", "black" };
            Array.Sort(colors);
            Array.Reverse(colors);

            Console.WriteLine(string.Join(",", colors));
        }
    }
}
